import PageNavbar from './components/PageNavbar';
import SignUp from './pages/SignUp';
import Login from './pages/Login';
import './App.css';


import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {

    const [user, setUser] = useState({
      // Allows us to store and to access the properties in the "user ID" and the "isAdmin" data.
      id: null,
      isAdmin: null
    })

  // Function for clearning localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      
      // User is logged in
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      
        // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <>
      {/*<UserProvider value={{user, setUser, unsetUser}}>*/}
      <Router>
        <PageNavbar />
        <Container>
          <Routes>
            {/*<Route path="/" element={<Home/>} />*/}    
            <Route path="/login" element={<Login/>} />
            <Route path="/register" element={<SignUp/>} />
            {/*<Route path="/logout" element={<Logout/>} />*/}
            {/*
                "*" - is the wildcard character that will match any path that has not already been matched by previous routes.
            
            {/*<Route path="*" element={<NotFound/>} />*/}*/}
          </Routes>
        </Container>
      </Router>
      {/*</UserProvider>*/}
    </>
  );
}

export default App;
