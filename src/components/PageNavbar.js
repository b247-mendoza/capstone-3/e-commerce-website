import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Offcanvas from 'react-bootstrap/Offcanvas';
import { useState, useEffect, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

export default function PageNavBar(){
	const { user } = UserContext(UserContext);

	return(

		<>
		    {[false, 'sm', 'md', 'lg', 'xl', 'xxl'].map((expand) => (
		        <Navbar key={expand} bg="light" expand={expand} className="mb-3">
		        <Container fluid>
		            <Navbar.Brand as={Link} to="/">Travels and Tours</Navbar.Brand>
		            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
		            <Navbar.Offcanvas
		              id={`offcanvasNavbar-expand-${expand}`}
		              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
		              placement="end"
		            >
		            <Offcanvas.Header closeButton>
		                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
		                  Offcanvas
		                </Offcanvas.Title>
		            </Offcanvas.Header>
		            <Offcanvas.Body>
		               <Nav className="justify-content-end flex-grow-1 pe-3">
		                 <Nav.Link as={Link} to="/login">Login</Nav.Link>
		                 <Nav.Link as={Link} to="/Register">Signup</Nav.Link>
		                </Nav>
		             </Offcanvas.Body>
		           </Navbar.Offcanvas>
		         </Container>
		       </Navbar>
		     ))}
		</>
	)
}