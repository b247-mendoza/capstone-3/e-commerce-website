import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Signup(){

	const { user, setUser } = useContext(UserContext);

	const [userName, setUserName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [cpassword, setCpassword] = useState("");

	const[isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function signupUser(e){
		fetch(`${process.env.ECOMMERCE_APP_API_URL}/register`,{
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				userName: userName,
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				Swal.fire({
					title: 'Signed Up Successfully!',
					icon: 'success',
					text: 'Try to Login'
				})
				setUserName("");
				setEmail("");
				setPassword("");
				setCpassword("");
				navigate("/login")
			} else {
				Swal.fire({
					title: 'Signed Up Failed!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		});
	};

	useEffect(() => {

		if((userName !== "" && email !== "" && password !== "" && password.length >= 9 && cpassword === password)) {
			setIsActive(true);
		}
	}, [userName, email, password, cpassword]);

	return (

		(user.id !== null) ?
			<Navigate to="/" />
		:

			<Form onSubmit={(e)=>Signup(e)}>
				<h1>Sign Up</h1>
				<Form.Group controlId="userName">
					<Form.Label>User Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter User Name"
						value={userName}
						onChange={e => setUserName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
					We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type = "password"
						placeholder = "Password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						Must be 9 characters or more.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="cpassword">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type = "password"
						placeholder = "Verify Password"
						value={cpassword}
						onChange={e => setCpassword(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ?	

					<Button variant="primary my-3" type="submit" id="submitBtn">Sign Up</Button>
				:
					<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Sign Up</Button>

				}		
			</Form>	
	);
};